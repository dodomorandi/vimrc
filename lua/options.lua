vim.g.filetype = true
vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.g.plugin = true
vim.g.syntax = true

vim.o.breakindent = true
vim.o.clipboard = "unnamedplus"
vim.o.cmdheight = 1
vim.o.compatible = false
vim.o.completeopt = "menu"
vim.o.expandtab = true
vim.o.hidden = true
vim.o.laststatus = 3
vim.o.number = true
vim.o.relativenumber = true
vim.o.secure = true
vim.o.shiftwidth = 4
vim.o.shortmess = "c"
vim.o.showmatch = true
vim.o.showtabline = 1
vim.o.signcolumn = "yes"
vim.o.softtabstop = 4
vim.o.spell = true
vim.o.spelllang = "en,it"
vim.o.termguicolors = true
vim.o.undofile = true
vim.o.updatetime = 250
vim.o.wrap = false
vim.o.foldlevel = 99999
vim.o.foldmethod = "expr"

vim.api.nvim_create_autocmd("BufReadPost", {
  callback = function()
    vim.defer_fn(function()
      vim.o.foldexpr = "v:lua.vim.treesitter.foldexpr()"
    end, 100)
  end,
})
