local textobjects = {
  select = {
    enable = true,
    lookahead = true,
    keymaps = {
      ["af"] = { query = "@function.outer", desc = "Select around a function region" },
      ["if"] = { query = "@function.inner", desc = "Select inner part of a function region" },
      ["ac"] = { query = "@class.outer", desc = "Select around a class region" },
      ["ic"] = { query = "@class.inner", desc = "Select inner part of a class region" },
      ["aa"] = { query = "@parameter.outer", desc = "Select around a [a]rgument parameter region" },
      ["ia"] = { query = "@parameter.inner", desc = "Select inner part of a [a]rgument parameter region" },
    },
  },
}

return {
  "nvim-treesitter/nvim-treesitter",
  event = "VeryLazy",
  dependencies = {
    "nvim-treesitter/nvim-treesitter-textobjects",
    "rescript-lang/tree-sitter-rescript",
  },
  build = ":TSUpdate",
  config = function()
    local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
    parser_config.rescript = {
      install_info = {
        url = "https://github.com/rescript-lang/tree-sitter-rescript",
        branch = "main",
        files = { "src/scanner.c" },
        generate_requires_npm = false,
        requires_generate_from_grammar = true,
        use_makefile = true,
      },
    }

    local configs = require "nvim-treesitter.configs"

    configs.setup {
      ensure_installed = {
        "c",
        "lua",
        "vim",
        "vimdoc",
        "query",
        "rust",
        "json",
        "javascript",
        "toml",
        "org",
        "rescript",
      },
      sync_install = false,
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = { "org" },
      },
      indent = { enable = false, disable = { "org" } },
      additional_vim_regex_highlighting = { "org" },
      modules = {},
      ignore_install = {},
      auto_install = true,
      textobjects = textobjects,
    }
  end,
}
