return {
  "nvim-orgmode/orgmode",
  config = function()
    require("orgmode").setup {
      org_agenda_files = { "~/Documenti/org/*" },
      org_default_notes_file = "~/Documenti/org/refile.org",

      require("util").create_filetype_augroup("org", function()
        vim.bo.textwidth = 100
      end),
    }
  end,
}
