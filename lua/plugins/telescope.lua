local function map(key, callback, desc)
  vim.keymap.set("n", key, callback, { desc = desc })
end

return {
  "nvim-telescope/telescope.nvim",
  event = "VimEnter",
  dependencies = {
    "nvim-lua/plenary.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
      cond = function()
        return vim.fn.executable "make" == 1
      end,
    },
    { "nvim-tree/nvim-web-devicons" },
  },
  config = function()
    require("telescope").setup {
      defaults = {
        path_display = { "truncate" },
      },
      extensions = {
        package_info = {
          theme = "cursor",
        },
      },
    }
    local builtin = require "telescope.builtin"

    map("<leader>ff", builtin.find_files, "[F]ind [F]iles")
    map("<leader>gf", builtin.git_files, "[G]it [F]iles")
    map("<leader>fg", builtin.live_grep, "[F]ile [G]rep")
    map("<leader>fmg", function()
      builtin.live_grep {
        additional_args = function()
          return { "--multiline" }
        end,
      }
    end, "[F]ile [M]ultiline [G]rep")
    map("<leader>fb", builtin.buffers, "[F]ile [B]uffers")
    map("<leader>bs", builtin.lsp_document_symbols, "[B]uffer [S]ymbols")
    map("<leader>qf", builtin.quickfix, "[Q]uick[F]ix")
    map("<leader>ll", builtin.loclist, "[L]ocation [L]ist")
    map("<leader>dt", builtin.tags, "[D]ocument [T]ags")
    map("<leader>bt", builtin.current_buffer_tags, "[B]uffer [T]ags")
    map("<leader>ht", builtin.help_tags, "[H]elp [T]ags")
    map("<leader>wd", builtin.diagnostics, "[W]orkspace [D]iagnostics")
    map("<leader>rr", builtin.resume, "[R]esume")
    map("<leader>ws", function()
      builtin.lsp_workspace_symbols { show_line = true }
    end, "[W]orkspace [S]ymbols")
    map("<leader>vf", function()
      builtin.find_files { cwd = vim.fn.stdpath "config", follow = true }
    end, "[V]im [F]iles")
  end,
}
