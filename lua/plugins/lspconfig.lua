local lsp_attach_group_id = vim.api.nvim_create_augroup("LspAttachCommon", {})
vim.api.nvim_create_autocmd("LspAttach", {
  group = lsp_attach_group_id,
  callback = function(args)
    if not (args and args.buf) then
      return
    end

    local bufnr = args.buf
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    local function buf_set_keymap(mode, lhs, callback)
      vim.keymap.set(mode, lhs, callback, { buffer = bufnr })
    end

    local ts_builtin = require "telescope.builtin"
    buf_set_keymap("n", "gD", vim.lsp.buf.declaration)
    buf_set_keymap("n", "gd", ts_builtin.lsp_definitions)
    buf_set_keymap("n", "K", vim.lsp.buf.hover)
    buf_set_keymap("n", "gi", ts_builtin.lsp_implementations)
    buf_set_keymap("n", "<C-k>", vim.lsp.buf.signature_help)
    buf_set_keymap("n", "<localleader>wa", vim.lsp.buf.add_workspace_folder)
    buf_set_keymap("n", "<localleader>wr", vim.lsp.buf.remove_workspace_folder)
    buf_set_keymap("n", "<localleader>wl", function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end)
    buf_set_keymap("n", "<localleader>D", vim.lsp.buf.type_definition)
    buf_set_keymap("n", "<localleader>rn", vim.lsp.buf.rename)
    buf_set_keymap("n", "<localleader>ca", vim.lsp.buf.code_action)
    buf_set_keymap("n", "gr", ts_builtin.lsp_references)
    buf_set_keymap("n", "<leader>e", function()
      vim.diagnostic.open_float { border = "rounded" }
    end)
    buf_set_keymap("n", "[g", function()
      vim.diagnostic.goto_prev {
        severity = { min = vim.diagnostic.severity.WARN },
        float = false,
      }
    end)
    buf_set_keymap("n", "]g", function()
      vim.diagnostic.goto_next {
        severity = { min = vim.diagnostic.severity.WARN },
        float = false,
      }
    end)
    buf_set_keymap("n", "<localleader>q", vim.fn.setloclist)
    buf_set_keymap("n", "<localleader>f", vim.lsp.buf.format)
    buf_set_keymap("n", "<leader>dd", function()
      ts_builtin.diagnostics { bufnr = bufnr }
    end)

    vim.b[bufnr].auto_format = true
    if client.server_capabilities.documentFormattingProvider then
      local group = vim.api.nvim_create_augroup("LspFormatBufWrite", {})
      vim.api.nvim_create_autocmd({ "BufWrite" }, {
        group = group,
        callback = function(args)
          if vim.b[args.buf].auto_format then
            vim.lsp.buf.format {
              bufnr = args.buf,
              group = group,
            }
          end
        end,
      })

      vim.api.nvim_create_autocmd({ "LspDetach" }, {
        group = group,
        callback = function()
          vim.api.nvim_del_augroup_by_id(group)
        end,
      })
    end
  end,
})

return {
  "neovim/nvim-lspconfig",
  event = "VeryLazy",
  config = require("lsp/setup").setup,
  dependencies = {
    "folke/neodev.nvim",
    "hrsh7th/cmp-nvim-lsp",
  },
}
