return {
  "vuki656/package-info.nvim",
  dependencies = {
    "MunifTanjim/nui.nvim",
  },
  config = function()
    require("package-info").setup {
      colors = {
        up_to_date = "#3C4048",
        outdated = "#d19a66",
      },
    }
    require("telescope").load_extension "package_info"
    vim.keymap.set("n", "<leader>pi", function()
      require("telescope.command").load_command "package_info"
    end)
  end,
}
