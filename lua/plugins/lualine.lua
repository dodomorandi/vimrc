local function orgmode_statusline()
  return _G.orgmode.statusline()
end

local function get_lualine_c()
  local lualine_c = { "filename" }
  local status, module = pcall(require, "lsp-progress")
  if status then
    table.insert(lualine_c, module.progress)
  end

  status, module = pcall(require, "package-info")
  if status then
    table.insert(lualine_c, module.get_status)
  end

  if _G.orgmode then
    table.insert(lualine_c, { orgmode_statusline })
  end
  return lualine_c
end

return {
  "nvim-lualine/lualine.nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    "linrongbin16/lsp-progress.nvim",
  },
  config = function()
    require("lualine").setup {
      options = {
        cons_enabled = true,
        theme = "kanagawa",
        globalstatus = false,
      },
      sections = {
        lualine_c = {
          {
            "filename",
            path = 1,
          },
        },
      },
      extensions = {
        "fugitive",
        "nerdtree",
        "quickfix",
      },
    }

    vim.api.nvim_create_augroup("lualine_augroup", { clear = true })
    vim.api.nvim_create_autocmd("User", {
      group = "lualine_augroup",
      pattern = "LspProgressStatusUpdated",
      callback = require("lualine").refresh,
    })
  end,
}
