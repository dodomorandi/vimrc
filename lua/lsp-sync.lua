local util = require "vim.lsp.util"

local M = {}

local wait_result_reason = { [-1] = "timeout", [-2] = "interrupted", [-3] = "error" }

---@private
--- Sends a request to the server and synchronously waits for the response.
---
--- This is a wrapper around {client.request}
---
---@param client lsp.Client
---@param method (string) LSP method name.
---@param params (table) LSP request params.
---@param timeout_ms (integer|nil) Maximum time in milliseconds to wait for
---                               a result. Defaults to 1000
---@param bufnr (integer) Buffer handle (0 for current).
---@return {err: lsp.ResponseError|nil, result:any, ctx:table, config:table}|nil, string|nil err # a dictionary, where
--- `err` and `result` come from the |lsp-handler|.
--- On timeout, cancel or error, returns `(nil, err)` where `err` is a
--- string describing the failure reason. If the request was unsuccessful
--- returns `nil`.
---@see vim.lsp.buf_request_sync()
local function request_sync(client, method, params, timeout_ms, bufnr)
  local request_result = nil
  local function _sync_handler(err, result, ctx, config)
    request_result = { err = err, result = result, ctx = ctx, config = config }
  end

  local success, request_id = client.request(method, params, _sync_handler, bufnr)
  if not success then
    return nil
  end

  local wait_result, reason = vim.wait(timeout_ms or 1000, function()
    return request_result ~= nil
  end, 10)

  if not wait_result then
    if request_id then
      client.cancel_request(request_id)
    end
    return nil, wait_result_reason[reason]
  end
  return request_result
end

--- Renames all references to the symbol under the cursor, **synchronously**.
---
---@param new_name string|nil If not provided, the user will be prompted for a new
---                name using |vim.ui.input()|.
---@param options table|nil additional options
---     - filter (function|nil):
---         Predicate used to filter clients. Receives a client as argument and
---         must return a boolean. Clients matching the predicate are included.
---     - name (string|nil):
---         Restrict clients used for rename to ones where client.name matches
---         this field.
---     - timeout_ms {string|nil}:
---         The timeout of the request, in milliseconds. If nil it defaults to
---         1000.
function M.rename(new_name, options)
  options = options or {}
  local bufnr = options.bufnr or vim.api.nvim_get_current_buf()
  ---@type lsp.Client[]
  local clients = vim.lsp.get_active_clients {
    bufnr = bufnr,
    name = options.name,
  }
  if options.filter then
    clients = vim.tbl_filter(options.filter, clients)
  end

  -- Clients must at least support rename, prepareRename is optional
  ---@type lsp.Client[]
  clients = vim.tbl_filter(function(client)
    return client.supports_method "textDocument/rename"
  end, clients)

  if #clients == 0 then
    vim.notify "[LSP] Rename, no matching language servers with rename capability."
  end

  local win = vim.api.nvim_get_current_win()

  -- Compute early to account for cursor movements after going async
  local cword = vim.fn.expand "<cword>"

  ---@private
  local function get_text_at_range(range, offset_encoding)
    return vim.api.nvim_buf_get_text(
      bufnr,
      range.start.line,
      util._get_line_byte_from_position(bufnr, range.start, offset_encoding),
      range["end"].line,
      util._get_line_byte_from_position(bufnr, range["end"], offset_encoding),
      {}
    )[1]
  end

  for _, client in pairs(clients) do
    local function rename(name)
      local params = util.make_position_params(win, client.offset_encoding)
      params.newName = name
      local result, err = request_sync(client, "textDocument/rename", params, options.timeout_ms, bufnr)
      local handler = client.handlers["textDocument/rename"] or vim.lsp.handlers["textDocument/rename"]
      handler(result.err, result.result, result.ctx, result.config)
    end

    if client.supports_method "textDocument/prepareRename" then
      local params = util.make_position_params(win, client.offset_encoding)
      local result, err = request_sync(client, "textDocument/prepareRename", params, options.timeout_ms, bufnr)
      if err or result == nil then
        local msg = err and ("Error on prepareRename: " .. (result and result.err.message or err or ""))
          or "Nothing to rename"
        vim.notify(msg, vim.log.levels.INFO)
        return
      end

      if new_name then
        rename(new_name)
      else
        local prompt_opts = {
          prompt = "New Name: ",
        }
        -- result: Range | { range: Range, placeholder: string }
        if result.placeholder then
          prompt_opts.default = result.placeholder
        elseif result.start then
          prompt_opts.default = get_text_at_range(result, client.offset_encoding)
        elseif result.range then
          prompt_opts.default = get_text_at_range(result.range, client.offset_encoding)
        else
          prompt_opts.default = cword
        end
        vim.ui.input(prompt_opts, function(input)
          if not input or #input == 0 then
            return
          end
          rename(input)
        end)
      end
    else
      assert(client.supports_method "textDocument/rename", "Client must support textDocument/rename")
      if new_name then
        rename(new_name)
      else
        local prompt_opts = {
          prompt = "New Name: ",
          default = cword,
        }
        vim.ui.input(prompt_opts, function(input)
          if not input or #input == 0 then
            return
          end
          rename(input)
        end)
      end
    end
  end
end

return M
