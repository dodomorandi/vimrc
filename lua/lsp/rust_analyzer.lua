local initial_extra_args

---Reloads Rust workspace
---@param client vim.lsp.Client
local function reload_workspace(client)
  client.request("rust-analyzer/reloadWorkspace", nil, function(err)
    if err then
      error(tostring(err))
    end
    vim.notify "Cargo workspace reloaded"
  end, 0)
end

--- @param args { args: string }
local function set_clippy_pedantic(args)
  local clients = vim.lsp.get_clients {
    name = "rust_analyzer",
    bufnr = vim.api.nvim_get_current_buf(),
  }

  if initial_extra_args == nil then
    initial_extra_args = {}
    for _, client in pairs(clients) do
      local extra_args = client.config.settings["rust-analyzer"].checkOnSave.extraArgs
      if extra_args == nil then
        extra_args = {}
      end

      table.insert(initial_extra_args, extra_args)
    end
  end

  for client_index, client in ipairs(clients) do
    local settings = client.config.settings
    local check_on_save = settings["rust-analyzer"].checkOnSave
    local extra_args = check_on_save.extraArgs
    if extra_args == nil then
      extra_args = {}
    end

    local new_extra_args = {}
    for key, value in pairs(initial_extra_args[client_index]) do
      new_extra_args[key] = value
    end
    if args.args == "true" then
      local has_doble_dash = false
      for index, value in ipairs(extra_args) do
        if value[index] == "--" then
          has_doble_dash = true
          break
        end
      end

      if not has_doble_dash then
        table.insert(new_extra_args, "--")
      end

      table.insert(new_extra_args, "-W")
      table.insert(new_extra_args, "clippy::pedantic")
    elseif args.args ~= "false" then
      error "argument to ClippyPedantic is invalid"
    end

    check_on_save.extraArgs = new_extra_args
    client.notify("workspace/didChangeConfiguration", {
      settings = settings,
    })
    reload_workspace(client)
  end
end

---Add one or more features to the set current ones
--- @param args { fargs: string[] }
local function add_features(args)
  local clients = vim.lsp.get_clients {
    name = "rust_analyzer",
    bufnr = vim.api.nvim_get_current_buf(),
  }

  for client_index, client in ipairs(clients) do
    local settings = client.config.settings
    if settings == nil then
      settings = {}
      client.config.settings = settings
    end

    if settings["rust-analyzer"] == nil then
      settings["rust-analyzer"] = {
        cargo = {
          features = {},
        },
      }
    end

    local cargo_settings = settings["rust-analyzer"]["cargo"]
    if cargo_settings == nil then
      settings["rust-analyzer"]["cargo"] = {
        features = {},
      }
      cargo_settings = settings["rust-analyzer"]["cargo"]
    end

    ---@type string[]
    local features = cargo_settings["features"]
    if features == nil then
      cargo_settings["features"] = {}
      features = cargo_settings["features"]
    end

    for _, feature in ipairs(args.fargs) do
      local already_set = false
      for _, current_feature in ipairs(features) do
        if current_feature == feature then
          already_set = true
          break
        end
      end

      if not already_set then
        table.insert(features, feature)
      end
    end

    settings["rust-analyzer"]["cargo"] = cargo_settings
    client.notify("workspace/didChangeConfiguration", {
      settings = settings,
    })
    reload_workspace(client)
  end
end

return {
  settings = {
    ["rust-analyzer"] = {
      checkOnSave = {
        command = "clippy",
      },
    },
  },
  on_attach = function()
    vim.api.nvim_create_user_command("ClippyPedantic", set_clippy_pedantic, {
      force = true,
      nargs = 1,
      complete = function()
        return { "true", "false" }
      end,
    })

    vim.api.nvim_create_user_command("RustAddFeatures", add_features, {
      force = true,
      nargs = "+",
    })
  end,
}
