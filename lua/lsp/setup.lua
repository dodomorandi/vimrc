local M = {}

local plugins = {
  "lua_ls",
  "rust_analyzer",
  "ts_ls",
  "eslint",
  "gopls",
  "clangd",
  "ruff",
  "jsonls",
  "tailwindcss",
}

--- @param client vim.lsp.Client
--- @param bufnr integer
local function on_attach(client, bufnr)
  if client.server_capabilities.inlayHintProvider then
    vim.lsp.inlay_hint.enable(false, { bufnr = bufnr })
  end
end

M.setup = function()
  local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()
  local lspconfig = require "lspconfig"

  for _, module_name in pairs(plugins) do
    local module = "lsp/" .. module_name
    local module_setup = require(module)

    if module_setup.capabilities then
      local merged_capabilities = vim.tbl_deep_extend("force", {}, lsp_capabilities, module_setup.capabilities)
      module_setup.capabilities = merged_capabilities
    else
      module_setup.capabilities = lsp_capabilities
    end

    if module_setup.on_attach then
      local setup_on_attach = module_setup.on_attach
      module_setup.on_attach = function(client, bufnr)
        setup_on_attach(client, bufnr)
        on_attach(client, bufnr)
      end
    else
      module_setup.on_attach = on_attach
    end

    local function toggleInlayHints()
      vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled { bufnr = 0 }, { bufnr = 0 })
    end

    vim.keymap.set("n", "<localleader>ih", toggleInlayHints, { desc = "Toggle [I]nlay [H]ints" })

    lspconfig[module_name].setup(module_setup)
  end
end

return M
