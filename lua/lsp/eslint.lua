return {
  on_attach = function(client, bufnr)
    local group = vim.api.nvim_create_augroup("EslintFixAllBufWrite_" .. bufnr, {})
    vim.api.nvim_create_autocmd({ "BufWrite" }, {
      buffer = bufnr,
      callback = function()
        vim.cmd [[ EslintFixAll ]]
      end,
      group = group,
    })

    vim.api.nvim_create_autocmd({ "LspDetach" }, {
      group = group,
      callback = function(args)
        vim.api.nvim_del_augroup_by_id(group)
      end,
    })
  end,
}
