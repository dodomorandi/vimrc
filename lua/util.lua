local M = {}

function M.create_filetype_augroup(filetype, callback)
  local group = vim.api.nvim_create_augroup("filetype_" .. filetype, {})
  vim.api.nvim_create_autocmd({ "Filetype" }, {
    pattern = filetype,
    callback = callback,
    group = group,
  })
end

return M
